package com.opp.OrganizacijaFestivala.view;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BarcodeQRCode;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.opp.OrganizacijaFestivala.model.RegisteredUser;
import com.opp.OrganizacijaFestivala.repository.ActivityRepository;
import com.opp.OrganizacijaFestivala.repository.EventRepository;
import com.opp.OrganizacijaFestivala.repository.FestivalRepository;
import com.opp.OrganizacijaFestivala.repository.JobActivityRepo;
import com.opp.OrganizacijaFestivala.repository.JobApplicationRepository;
import com.opp.OrganizacijaFestivala.repository.JobRepository;
import com.opp.OrganizacijaFestivala.service.EventService;
import com.opp.OrganizacijaFestivala.service.FestivalService;
import com.opp.OrganizacijaFestivala.service.JobActivityService;
import com.opp.OrganizacijaFestivala.service.JobAppService;
import com.opp.OrganizacijaFestivala.service.JobService;
import com.opp.OrganizacijaFestivala.model.Event;
import com.opp.OrganizacijaFestivala.model.Job;
import com.opp.OrganizacijaFestivala.model.JobApplication;
import com.opp.OrganizacijaFestivala.model.Festival;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;

import com.itextpdf.text.Image;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class PdfView extends AbstractPdfView {
	@Autowired
    EventService eventService;
	@Autowired
	JobService	jobService;
	@Autowired
	FestivalService festivalService;
	@Autowired
    JobRepository jobRepository;

    @Autowired
    ActivityRepository activityRepository;

    @Autowired
    EventRepository eventRepository;

    @Autowired
    JobActivityRepo jobActivityRepo;
    

    @Autowired
    JobActivityService jobActivityService;
    @Autowired
    JobApplicationRepository jobApplicationRepository;
    @Autowired
    JobAppService jobAppService;
    @Autowired
    FestivalRepository festivalRepository;
    @Override
    protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer, HttpServletRequest request, HttpServletResponse response) throws Exception {
        // change the file name
        response.setHeader("Content-Disposition", "attachment; filename=\"Propusnica.pdf\"");
        
        

 
        final RegisteredUser user=(RegisteredUser) model.get("user");
        final Festival festival=(Festival) model.get("festival");
        final Event event=(Event) model.get("dogadaj"); 
        final JobApplication posao=(JobApplication) model.get("posel");
        final Job pos=(Job) model.get("posao");
        
        
        String logo = "http://envy.de/img/kunden/42/ultraeurope_logo_envy.jpg";
        Image image = Image.getInstance(logo);
        image.scalePercent(20);
         
        String faca="https://images-na.ssl-images-amazon.com/images/I/61%2BhgRqKLEL._SX425_.jpg";
        Image image2 = Image.getInstance(faca);
        image2.scalePercent(30);
        
        BarcodeQRCode barcodeQRCode;
        if(event==null)
        {
        	barcodeQRCode = new BarcodeQRCode(user.getUsername().concat(Long.toString(festival.getId())), 10, 10, null);
        }
        else
        {
        	barcodeQRCode = new BarcodeQRCode(user.getUsername()+Long.toString(festival.getId()+posao.getNumPrinted()+pos.getOrdinalNumber()), 10, 10, null);
        }
        
        Image codeQrImage = barcodeQRCode.getImage();
        codeQrImage.scaleAbsolute(100, 100);
       
        
        PdfPTable table = new PdfPTable(2);
        PdfPTable table1= new PdfPTable(2);
        PdfPTable table2= new PdfPTable(2);
        PdfPTable table3= new PdfPTable(2);
        PdfPTable table4= new PdfPTable(2);
        PdfPTable table5= new PdfPTable(2);
        
        table.setTotalWidth(10);
        table1.setTotalWidth(10);
        table2.setTotalWidth(10);
        table3.setTotalWidth(10);
        table4.setTotalWidth(10);
        table5.setTotalWidth(10);
        
        
        
        //table.addCell(new Paragraph("Slika: "));
        //table.addCell(image2);
        table1.addCell(new Paragraph("Ime: "));
        table1.addCell(user.getFirst_name());
        table2.addCell(new Paragraph("Prezime: "));
        table2.addCell(user.getLast_name());
        table3.addCell(new Paragraph("Naziv festivala: "));
        table3.addCell(festival.getFestivalName());
        //table4.addCell(new Paragraph("Logo festivala: "));
        //table4.addCell(image);
        table5.addCell(new Paragraph("QR kod: "));
        table5.addCell(codeQrImage);
        
        
        
      
        
        
        //document.add(codeQrImage);
        //document.add(table);
        document.add(table1);
        document.add(table2);
        document.add(table3);
        //document.add(table4);
        document.add(table5);
        
        if(event!=null)
        {
        	
        	PdfPTable table6 = new PdfPTable(2);
        	table6.setTotalWidth(10);
        	table6.addCell(new Paragraph("Vrijeme pocetka: "));
            table6.addCell(event.getStartTime().toString());
            document.add(table6);
            
            PdfPTable table7 = new PdfPTable(2);
        	table7.setTotalWidth(10);
        	table7.addCell(new Paragraph("Vrijeme zavrsetka: "));
            table7.addCell(event.getEndTime().toString());
            document.add(table7);
            
            PdfPTable table8 = new PdfPTable(2);
        	table8.setTotalWidth(10);
        	table8.addCell(new Paragraph("Osoba broj: "));
        	table8.addCell(Integer.toString(posao.getNumPrinted())+"/"+Integer.toString(posao.getNumWorkers()));
        	document.add(table8);
        	//jobApplicationRepository.save(posao);
        	//jobAppService.updateNumPrinted(posao.getId());
        }
        
    }
}
