package com.opp.OrganizacijaFestivala.rest.controller;

import com.opp.OrganizacijaFestivala.model.Event;
import com.opp.OrganizacijaFestivala.model.Job;
import com.opp.OrganizacijaFestivala.model.RegisteredUser;
import com.opp.OrganizacijaFestivala.rest.dto.EventDTO;
import com.opp.OrganizacijaFestivala.rest.dto.EventDTO2;
import com.opp.OrganizacijaFestivala.rest.dto.JobActivityDTO;
import com.opp.OrganizacijaFestivala.service.EventService;
import com.opp.OrganizacijaFestivala.service.JobService;
import com.opp.OrganizacijaFestivala.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/events")
public class EventController {
    @Autowired
    private EventService eventService;

    @Autowired
    private UserService userService;

    @Autowired
    private JobService jobService;

    @PostMapping("/create")
    public Event createFestival(@RequestBody EventDTO eventDTO) {
        return  eventService.createEvent(eventDTO);
    }

    @GetMapping("/listEvents")
    public List<Event> getAllEvents(){
        return eventService.findAll();
    }


    //DOHVAĆANJE POSLOVA ZA NEKO DOGAĐANJE
    @GetMapping(value = "jobs/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Job> getEventJobs(@PathVariable(value = "id") Long id){
        return jobService.eventJobs(id);
    }

    //DOHVAĆANJE POSLOVA I NJIHOVIH AKTIVNOSTI ZA NEKO DOGAĐANJE
    @GetMapping(value = "jobsAndActivities/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<JobActivityDTO> getEventJobsAndActivities(@PathVariable(value = "id") Long id){
        return jobService.getJobsAndActivities(id);
    }

    //DOHVAĆANJE DOGAĐANJA PREKO ID
    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Event getEvent(@PathVariable(value = "id") Long id){
        return eventService.findById(id);
    }


    @DeleteMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(@PathVariable(value = "id") Long id){
        eventService.delete(id);
    }


    @GetMapping("/byorganizer/{organizerId}")
    public List<EventDTO2> getActiveEventsByOrganizer(@PathVariable Long organizerId) {
        LocalDateTime dateTime = LocalDateTime.now().plusHours(1);
        List<EventDTO2> list = new ArrayList<>();
        for (Event e : eventService.findAllByOrganizerAndIsActive(userService.findById(organizerId), true)) {
            EventDTO2 eventDTO2 = new EventDTO2(e.getId(), e.getEventName());
            list.add(eventDTO2);
        }
        return list;
    }

    @GetMapping("/byorganizerOld/{organizerId}")
    public List<EventDTO2> getOldEventsByOrganizer(@PathVariable Long organizerId) {
        LocalDateTime dateTime = LocalDateTime.now().plusHours(1);
        List<EventDTO2> list = new ArrayList<>();
        for (Event e : eventService.findAllByOrganizerAndIsActive(userService.findById(organizerId), false)) {
            EventDTO2 eventDTO2 = new EventDTO2(e.getId(), e.getEventName());
            list.add(eventDTO2);
        }
        return list;
    }
}
