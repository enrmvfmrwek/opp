package com.opp.OrganizacijaFestivala.rest.dto;

import com.opp.OrganizacijaFestivala.model.Activity;
import com.opp.OrganizacijaFestivala.model.Job;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

//služi za prijenos informacija o poslu u frontend (ime eventa, lista aktivnosti)
public class JobDTO2 {
    private long jobId;
    private int ordinalNumber;
    private Long winAppId;
    private long eventId;
    private String eventName;
    private LocalDateTime auctionEndTime;
    private String activityNames="";
    private String festivalName="";

    public JobDTO2(){
        super();
    }

    public  JobDTO2(Job job, String eventName, String festivalName, List<Activity> activities){
        jobId = job.getJobId();
        ordinalNumber = job.getOrdinalNumber();
        winAppId = job.getWinAppId();
        eventId = job.getEventId();
        this.eventName = eventName;
        auctionEndTime = job.getAuctionEndTime();
        this.festivalName = festivalName;
        for(Activity activity: activities){
           activityNames += activity.getActivityName()+", ";
        }
        if(activityNames.length()>0){
            activityNames = activityNames.substring(0, activityNames.length() - 2);
        }

    }


    public String getFestivalName() {
        return festivalName;
    }

    public void setFestivalName(String festivalName) {
        this.festivalName = festivalName;
    }

    public long getJobId() {
        return jobId;
    }

    public void setJobId(long jobId) {
        this.jobId = jobId;
    }

    public int getOrdinalNumber() {
        return ordinalNumber;
    }

    public void setOrdinalNumber(int ordinalNumber) {
        this.ordinalNumber = ordinalNumber;
    }

    public Long getWinAppId() {
        return winAppId;
    }

    public void setWinAppId(Long winAppId) {
        this.winAppId = winAppId;
    }

    public long getEventId() {
        return eventId;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public LocalDateTime getAuctionEndTime() {
        return auctionEndTime;
    }

    public void setAuctionEndTime(LocalDateTime auctionEndTime) {
        this.auctionEndTime = auctionEndTime;
    }

    public String getActivityNames() {
        return activityNames;
    }

    public void setActivityNames(String activityNames) {
        this.activityNames = activityNames;
    }
}
