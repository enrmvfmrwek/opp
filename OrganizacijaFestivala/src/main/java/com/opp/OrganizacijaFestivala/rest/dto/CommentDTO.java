package com.opp.OrganizacijaFestivala.rest.dto;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

public class CommentDTO {
    @NotNull
    private Long profileId;

    @NotNull
    private Long commenterId;

    @NotNull
    private String comment;

    @NotNull
    private Long jobId;

    public Long getProfileId() {
        return profileId;
    }

    public void setProfileId(Long profileId) {
        this.profileId = profileId;
    }

    public Long getCommenterId() {
        return commenterId;
    }

    public void setCommenterId(Long commenterId) {
        this.commenterId = commenterId;
    }



    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }
}
