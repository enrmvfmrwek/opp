package com.opp.OrganizacijaFestivala.rest.controller;

import com.opp.OrganizacijaFestivala.model.Event;
import com.opp.OrganizacijaFestivala.model.Festival;
import com.opp.OrganizacijaFestivala.rest.dto.FestivalDTO;
import com.opp.OrganizacijaFestivala.rest.dto.FestivalDTO2;
import com.opp.OrganizacijaFestivala.service.EventService;
import com.opp.OrganizacijaFestivala.service.FestivalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/festivals")
public class FestivalController {
    @Autowired
    private FestivalService festivalService;

    @Autowired
    private EventService eventService;

    @PostMapping("/create")
    @Secured("ROLE_LEADER")
    public Festival createFestival(@RequestBody FestivalDTO festivalDTO) {
        return  festivalService.createFestival(festivalDTO);
    }

    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Festival getById(@PathVariable(value = "id") Long id){
        return festivalService.findById(id);
    }

    @GetMapping("/listFestivals")
    public List<Festival> getAllFestivals(){
        return festivalService.findAll();
    }

    @GetMapping("/listActiveFestivals")
    public List<Festival> getAllActiveFestivals(){
        return festivalService.findAllByActive(true);
    }

    @GetMapping("/listFinishedFestivals")
    public List<Festival> getAllInactiveFestivals(){
        return festivalService.findAllByActive(false);
    }


    @GetMapping(value = "festivalAndFinished/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public FestivalDTO2 getById2(@PathVariable(value = "id") Long id){
        return festivalService.findById2(id);
    }


    @DeleteMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(@PathVariable(value = "id") Long id){

        festivalService.delete(id);
    }

    //DOHVAĆANJE DOGAĐANJA ZA NEKI FESTIVAL
    @GetMapping(value = "events/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Event> getFestivalEvents(@PathVariable(value = "id") Long id){
        return eventService.festivalEvents(id);
    }

    //ZA DOHVAĆANJE PRIJAVA ZA ORGANIZACIJU KORISTI SE ORGAPPCONTROLLER

    @GetMapping("/{festival_id}")
    public Festival getFestivalById(@PathVariable Long festival_id) {
        return festivalService.findById(festival_id);
    }
}
