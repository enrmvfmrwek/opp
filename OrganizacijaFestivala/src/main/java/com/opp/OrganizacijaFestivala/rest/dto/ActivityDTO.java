package com.opp.OrganizacijaFestivala.rest.dto;


import org.jetbrains.annotations.NotNull;


public class ActivityDTO {

	@NotNull
 	private String  activity_name;

 	public String getActivityName()
 	{
 		return this.activity_name;
 	}

 	public void setActivityName(String activity_name)
 	{
 		this.activity_name=activity_name;
 	}
	
}
