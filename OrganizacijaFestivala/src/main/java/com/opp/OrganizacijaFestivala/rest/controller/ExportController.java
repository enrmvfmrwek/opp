package com.opp.OrganizacijaFestivala.rest.controller;

import com.opp.OrganizacijaFestivala.service.UserService;
import com.opp.OrganizacijaFestivala.model.Event;
import com.opp.OrganizacijaFestivala.model.Job;
import com.opp.OrganizacijaFestivala.model.JobApplication;
import com.opp.OrganizacijaFestivala.model.RegisteredUser;
import com.opp.OrganizacijaFestivala.repository.JobApplicationRepository;
import com.opp.OrganizacijaFestivala.service.EventService;
import com.opp.OrganizacijaFestivala.service.FestivalService;
import com.opp.OrganizacijaFestivala.service.JobAppService;
import com.opp.OrganizacijaFestivala.service.JobService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ExportController {

    @Autowired
    private UserService userService;
    
    @Autowired
    private FestivalService festivalService;
    
    @Autowired
    private EventService eventService;
    
    @Autowired
    private JobAppService jobAppService;
    
    @Autowired
    private JobService jobService;
    
    @Autowired
    JobApplicationRepository jobApplicationRepository;
    
    @GetMapping("/download")
    public String download(Model model, @RequestParam(value="festivalId", required=true) String festivalId,
    		@RequestParam(value="userId", required=true) String userId) {
    	
    	
    	Long festId=Long.parseLong(festivalId);
    	Long usId=Long.parseLong(userId);
    	model.addAttribute("user", userService.findById(usId));
        model.addAttribute("festival", festivalService.findById(festId));
        
        return "";
    }
    
    @GetMapping("/downloadIZV")
    public String downloadIZV(Model model, @RequestParam(value="appId", required=true) String appId)
    {
    	Long app_id=Long.parseLong(appId);
    	JobApplication ova=jobAppService.findById(app_id);
    	RegisteredUser us=ova.getPerformer();
    	Long ovajPosao=ova.getJobId();
    	Job posao=jobService.getJobById(ovajPosao);
    	Long evId=posao.getEventId();
    	Long usId=us.getId();
    	Event ovaj=eventService.findById(evId);
    	Long festId=ovaj.getFestivalId();
    	model.addAttribute("dogadaj", eventService.findById(evId));
    	model.addAttribute("user", userService.findById(usId));
    	model.addAttribute("festival", festivalService.findById(festId));
    	model.addAttribute("posel", ova);
    	model.addAttribute("appId", app_id);
    	model.addAttribute("posao", posao);
    	
    	ova.setNumPrinted(ova.getNumPrinted()+1);
    	jobApplicationRepository.saveAndFlush(ova);
    	return "";
    }


}
