package com.opp.OrganizacijaFestivala.rest.controller;

import com.opp.OrganizacijaFestivala.model.Comment;
import com.opp.OrganizacijaFestivala.rest.dto.CommentDTO;
import com.opp.OrganizacijaFestivala.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/comments")
public class CommentController {
    @Autowired
    CommentService commentService;

    @GetMapping(value = "profile/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Comment> getProfileComments(@PathVariable(value = "id") Long id){
        return commentService.getProfileComments(id);
    }

    @GetMapping(value = "commenter/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Comment> getCommenterComments(@PathVariable(value = "id") Long id){
        return commentService.getCommenterComments(id);
    }

    @PostMapping("/create")
    public Comment createComment(@RequestBody CommentDTO commentDTO){
        return commentService.create(commentDTO);
    }

    @DeleteMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteComment(@PathVariable(value = "id") Long id){
        commentService.delete(id);
    }

}
