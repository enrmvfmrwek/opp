package com.opp.OrganizacijaFestivala.rest.dto;

public class EventDTO2 {

    private Long id;
    private String eventName;

    public EventDTO2(Long id, String eventName) {
        this.id = id;
        this.eventName = eventName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }
}
