package com.opp.OrganizacijaFestivala.rest.controller;

import com.opp.OrganizacijaFestivala.model.Activity;
import com.opp.OrganizacijaFestivala.model.Festival;
import com.opp.OrganizacijaFestivala.model.Performer_Activity;
import com.opp.OrganizacijaFestivala.model.RegisteredUser;
import com.opp.OrganizacijaFestivala.rest.dto.FestivalDTO2;
import com.opp.OrganizacijaFestivala.rest.dto.LoginDTO;
import com.opp.OrganizacijaFestivala.rest.dto.UserDTO;
import com.opp.OrganizacijaFestivala.service.FestivalService;
import com.opp.OrganizacijaFestivala.service.PerformerActivityService;
import com.opp.OrganizacijaFestivala.service.UserService;
import com.opp.OrganizacijaFestivala.exception.EmailExistsException;
import com.opp.OrganizacijaFestivala.exception.InvalidRoleException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.json.JSONObject;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    private UserService service;

    @Autowired
    private FestivalService festivalService;

    @Autowired
    private PerformerActivityService performerActivityService;

    //LOGIN
    @PostMapping("/login")
    public String login(@RequestBody LoginDTO loginDTO){

        String token = service.login(loginDTO.getUsername(), loginDTO.getPassword());
        //System.out.println("TOKEN: "+token);
        /*JSONObject json = new JSONObject();
        json.put("token", token);
        json.put("username", loginDTO.getUsername());*/
        return "{\"token\": \""+token+"\", \"username\": \""+loginDTO.getUsername() +"\"}";
    }

/*
    //REGISTRACIJA
    @GetMapping("/registration")
    public String showRegistrationForm(WebRequest request, Model model) {
        UserDTO userDto = new UserDTO();
        model.addAttribute("user", userDto);
        return "registration";
    }
*/
    @PostMapping("/registration")
    public String registerUserAccount(@RequestBody UserDTO userDTO) throws EmailExistsException, InvalidRoleException {
        String token = null;

        if (!userDTO.checkRole()) {
            throw new InvalidRoleException(userDTO.getRole() + " nije prepoznata uloga!");
        }
        else if (userDTO.getRole().equals("voditelj")) {
            userDTO.setRole("nepotvrdjeni_voditelj");
        }

        return  service.registerNewUserAccount(userDTO);
    }


    //DOHVACANJE I POTVRDA VODITELJA
    @GetMapping("/leads")
    @Secured("ROLE_ADMIN")
    public List<RegisteredUser> listLeads(){
        return service.findAllByRole("nepotvrdjeni_voditelj");

    }

    @PutMapping(value = "leads/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Secured("ROLE_ADMIN")
    public RegisteredUser confirmLead(@PathVariable(value = "id") Long id){
        return service.updateUser(id);
    }

    //BRISANJE KORISNIKA
    @DeleteMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Secured("ROLE_ADMIN")
    public void deleteUser(@PathVariable(value = "id") Long id){
        service.delete(id);
    }

    //DOHVAĆANJE SVIH KORISNIKA
    @GetMapping("/listUsers")

    public List<RegisteredUser> getAllUsers(){
        return service.findAll();
    }

    //DOHVAĆANJE TRENUTNOG KORISNIKA
    @GetMapping("/currentUser")
    public RegisteredUser getCurrentUser(@RequestParam String token){
        //System.out.println(token);
        return service.currentUser(token);

    }

    //DOHVAĆANJE SVIH KORISNIKA ZA VODITELJA
    @GetMapping("/listUsersForLeads")
    @Secured("ROLE_LEADER")
    public List<RegisteredUser> getAllUsersForLeads() { List<RegisteredUser> newList = service.findAllByRole("organizator");
        newList.addAll(service.findAllByRole("izvodjac"));
        return newList;}

    //DOHVAĆANJE SVIH KORISNIKA ZA ORGANIZATORA
    @GetMapping("/listUsersForOrganizers")
    @Secured("ROLE_ORGANIZER")
    public List<RegisteredUser> getAllUsersForOrganizers(){
        return service.findAllByRole("izvodjac");
    }


    //DOHVAĆANJE FESTIVALA ZA NEKOG VODITELJA
    @GetMapping(value = "festivals/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public  List<Festival> getLeaderFestival(@PathVariable Long id){
        return festivalService.findAllByLeader(id);
    }

    //DOHVAĆANJE FESTIVALA I STANJA (AKTIVAN/ZAVRŠEN) ZA NEKOG VODITELJA
    @GetMapping(value = "festivals2/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public  List<FestivalDTO2> getLeaderFestival2(@PathVariable Long id){
        return festivalService.findAllByLeader2(id);
    }


    //DODAVANJE POSTOJEĆE DJELATNOSTI IZVOĐAČU
    @PostMapping("/activities")
    @Secured("ROLE_PERFORMER")
    public Performer_Activity addActivity(@RequestParam Long idP, @RequestParam Long idA){
        return performerActivityService.create(idP, idA);
    }

    //DOHVAĆANJE DJELATNOSTI NEKOG IZVOĐAČA
    @GetMapping(value = "activities/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Activity> getActivities(@PathVariable Long id) {
        return performerActivityService.findAllByPerformerId(id);
    }

    //MICANJE DJELATNOSTI S PROFILA IZVOĐAČA
    @DeleteMapping("/activities")
    public void removeActivity(@RequestParam Long idP, @RequestParam Long idA){
        performerActivityService.delete(idP, idA);
    }

    //DOHVAĆANJE KORISNIKA PREKO NJEGOVOG KORISNIČKOG IMENA
    @GetMapping(value = "{username}")
    @ResponseStatus(HttpStatus.OK)
    public RegisteredUser getUserWithUsername(@PathVariable(value = "username") String username){
        return service.findByUsername(username);

    }

}
