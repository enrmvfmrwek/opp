package com.opp.OrganizacijaFestivala.repository;

import com.opp.OrganizacijaFestivala.model.Performer_Activity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PerformerActivityRepo extends JpaRepository<Performer_Activity, Long> {

    List<Performer_Activity> findAllByPerformerId(Long id);
    List<Performer_Activity> findAllByActivityId(Long id);
    List<Performer_Activity> findAllByPerformerIdAndActivityId(Long pId, Long aId);
}
