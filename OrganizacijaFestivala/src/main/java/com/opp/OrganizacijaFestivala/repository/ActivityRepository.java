package com.opp.OrganizacijaFestivala.repository;

import com.opp.OrganizacijaFestivala.model.Activity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface ActivityRepository extends JpaRepository<Activity, Long>
{
	List<Activity> findAll();
	
	Activity findByActivityName(String activity_name);
	Activity findByActivityId(Long activity_id);
	
	@Transactional
	void deleteById(Long id);
	
	boolean existsById(Long id);
}
