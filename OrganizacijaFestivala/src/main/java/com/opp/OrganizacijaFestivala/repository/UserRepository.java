package com.opp.OrganizacijaFestivala.repository;

import com.opp.OrganizacijaFestivala.model.RegisteredUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<RegisteredUser, Long> {
    List<RegisteredUser> findAllByRole(String role);

    RegisteredUser findByEmail(String email);

    RegisteredUser findByUsername(String username);
    @Transactional
    void deleteByUsername(String username);

    boolean existsByUsername(String username);

}
