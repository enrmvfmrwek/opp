package com.opp.OrganizacijaFestivala.repository;

import com.opp.OrganizacijaFestivala.model.Job;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface JobRepository extends JpaRepository<Job, Long> {

    List<Job> findAllByEventId(Long id);

    List<Job> findAllByAuctionOpen(boolean open);

    List<Job> findAllByEventIdAndOrdinalNumber(Long eventId, int ordNum);
}
