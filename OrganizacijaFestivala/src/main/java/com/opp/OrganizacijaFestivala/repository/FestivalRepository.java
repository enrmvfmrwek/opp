package com.opp.OrganizacijaFestivala.repository;

import com.opp.OrganizacijaFestivala.model.Festival;
import com.opp.OrganizacijaFestivala.model.RegisteredUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface FestivalRepository extends JpaRepository<Festival, Long> {

    List<Festival> findAll();

    List<Festival> findAllByLeader(RegisteredUser leader);

    @Transactional
    void deleteById(Long id);

    boolean existsById(Long id);

}

