package com.opp.OrganizacijaFestivala.model;

import com.opp.OrganizacijaFestivala.rest.dto.JobDTO;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Job {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long jobId;


    @NotNull
    private int ordinalNumber;


    private Long winAppId;


    @NotNull
    private long eventId;

    @NotNull
    //po defaultu je zatvorena, naknadno organizator otvara
    private boolean auctionOpen = false;

    //moze biti null dok se ne otvori licitacija
    private LocalDateTime auctionEndTime;


    public Job(JobDTO jobDTO){
        ordinalNumber = jobDTO.getOrdinalNumber();
        eventId = jobDTO.getEventId();
    }

    public Job() {
        super();
    }

    public long getJobId() {
        return jobId;
    }

    public long getEventId() {
        return eventId;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

    public int getOrdinalNumber() {
        return ordinalNumber;
    }

    public void setOrdinalNumber(int ordinalNumber) {
        this.ordinalNumber = ordinalNumber;
    }

    public Long getWinAppId() {
        return winAppId;
    }

    public void setWinAppId(Long winAppId) {
        this.winAppId = winAppId;
    }

    public boolean isAuctionOpen() {
        return auctionOpen;
    }

    public void setAuctionOpen(boolean auctionOpen) {
        this.auctionOpen = auctionOpen;
    }

    public LocalDateTime getAuctionEndTime() {
        return auctionEndTime;
    }

    public void setAuctionEndTime(LocalDateTime auctionEndTime) {
        this.auctionEndTime = auctionEndTime;
    }

    @Override
    public String toString() {
        return "Posao #" + " " + jobId + " " + ordinalNumber+" "+auctionOpen+" "+auctionEndTime;
    }


}
