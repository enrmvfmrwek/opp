package com.opp.OrganizacijaFestivala.model;

import com.opp.OrganizacijaFestivala.rest.dto.FestivalDTO;
import javax.validation.constraints.NotNull;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Festival {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @ManyToOne
    private RegisteredUser leader;

    @NotNull
    private String festivalName;

    private String description;
    @NotNull
    private LocalDateTime startTime;
    @NotNull
    private LocalDateTime endTime;

    private Byte[] logo;



    public Festival(){
        super();
    }

    public Festival(FestivalDTO dto, RegisteredUser leader){
        this.leader = leader;
        festivalName = dto.getFestivalName();
        description = dto.getDescription();
        startTime = dto.getStartTime();
        endTime = dto.getEndTime();
        logo = dto.getLogo();
    }

    public Byte[] getLogo() {
        return logo;
    }

    public void setLogo(Byte[] logo) {
        this.logo = logo;
    }

    public Long getId() {
        return id;
    }

    public RegisteredUser getLeader() {
        return leader;
    }

    public void setLeader(RegisteredUser leader) {
        this.leader = leader;
    }

    public String getFestivalName() {
        return festivalName;
    }

    public void setFestivalName(String festivalName) {
        this.festivalName = festivalName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }
}
