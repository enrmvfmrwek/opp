package com.opp.OrganizacijaFestivala.model;


import com.opp.OrganizacijaFestivala.security.MyUserDetailsService;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
public class RegisteredUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    @NotNull
    private String username;
    @NotNull
    private String first_name;
    @NotNull
    private String last_name;
    @NotNull
    private String phone_number;
    @NotNull
    @Column(unique = true)
    private String email;
    @NotNull
    private String role;
    @NotNull
    private String password;
    private Byte[] profilePicture;

    public RegisteredUser() {
    }

    public RegisteredUser(String username, String role) {
        this.username = username;
        this.role = role;
    }

    public Byte[] getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(Byte[] profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getId() {
        return id;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Korisnik #" + " " + id + " " + username + " " + first_name + " " + last_name + " " +
                phone_number + " " + " " + email + " " + role;
    }

    public List<GrantedAuthority> getRoleAuthorities() {
        return MyUserDetailsService.getAuthorities(username,
                this.getRole());
    }
}

