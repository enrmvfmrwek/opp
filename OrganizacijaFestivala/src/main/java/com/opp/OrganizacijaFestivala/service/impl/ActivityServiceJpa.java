package com.opp.OrganizacijaFestivala.service.impl;

import com.opp.OrganizacijaFestivala.model.Job_Activity;
import com.opp.OrganizacijaFestivala.model.Performer_Activity;
import com.opp.OrganizacijaFestivala.repository.ActivityRepository;
import com.opp.OrganizacijaFestivala.repository.JobActivityRepo;
import com.opp.OrganizacijaFestivala.repository.PerformerActivityRepo;
import com.opp.OrganizacijaFestivala.repository.UserRepository;
import com.opp.OrganizacijaFestivala.model.Activity;
import com.opp.OrganizacijaFestivala.exception.CustomException;
import com.opp.OrganizacijaFestivala.exception.EmailExistsException;
import com.opp.OrganizacijaFestivala.security.JwtTokenProvider;
import com.opp.OrganizacijaFestivala.security.MyUserDetailsService;
import com.opp.OrganizacijaFestivala.service.ActivityService;
import com.opp.OrganizacijaFestivala.service.JobActivityService;
import com.opp.OrganizacijaFestivala.service.PerformerActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import com.opp.OrganizacijaFestivala.*;
import com.opp.OrganizacijaFestivala.rest.dto.ActivityDTO;

import javax.servlet.http.HttpServletRequest;
import java.text.Normalizer;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

@Service
public class ActivityServiceJpa implements ActivityService
{
	@Autowired
    ActivityRepository repository;

	@Autowired
	JobActivityRepo jobActivityRepo;
	@Autowired
	JobActivityService jobActivityService;
	@Autowired
	PerformerActivityService performerActivityService;
	@Autowired
	PerformerActivityRepo performerActivityRepo;
	
	@Override
	public List<Activity> findAllActivities()
	{
		return repository.findAll();
	}
	
	@Override
	public Activity findByActivityName(String activity_name)
	{
		Activity act = repository.findByActivityName(activity_name);
		if (act == null) {
            throw new CustomException("The activity doesn't exist", HttpStatus.NOT_FOUND);
        }
        return act;
	}

	@Override
	public Activity findByActivityId(Long activity_id)
	{
		Activity act=repository.findByActivityId(activity_id);
		if (act == null) {
            throw new CustomException("The activity doesn't exist", HttpStatus.NOT_FOUND);
        }
        return act;
	}
	
	@Override
    public boolean existsById(Long id) {
        return repository.existsById(id);
    }

	
	@Override
	public void deleteById(Long activity_id) {
		List<Job_Activity> job_activities = jobActivityRepo.findAllByActivityId(activity_id);
		for(Job_Activity job_activity: job_activities){
			jobActivityService.delete(job_activity);
		}
		List<Performer_Activity> performer_activities = performerActivityRepo.findAllByActivityId(activity_id);
		for(Performer_Activity performer_activity: performer_activities){
			performerActivityService.delete(performer_activity);
		}
		repository.deleteById(activity_id);
		
	}
	
    public Activity createActivity(String name) {
		Assert.notNull(name, "Ime djelatnosti mora biti zadano");
		Assert.isTrue(name.length()>0, "Ime djelatnosti mora biti zadano");
		//makne dijakritičke znakove iz imena djelatnosti jer ih baza ne podržava
		String convertedString =
				Normalizer
						.normalize(name, Normalizer.Form.NFD)
						.replaceAll("[^\\p{ASCII}]", "");
		System.out.println(convertedString);
        Assert.isNull(repository.findByActivityName(convertedString), "Ta djelatnost već postoji");
		Activity activity = new Activity(convertedString);
        return repository.save(activity);
    }



	
}
