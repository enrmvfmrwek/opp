package com.opp.OrganizacijaFestivala.service;

import com.opp.OrganizacijaFestivala.model.Comment;
import com.opp.OrganizacijaFestivala.rest.dto.CommentDTO;

import java.util.List;

public interface CommentService {
    List<Comment> getProfileComments(Long id);
    List<Comment> getCommenterComments(Long id);
    Comment create(CommentDTO commentDTO);
    void delete(Comment comment);
    void delete(Long commentId);
}
