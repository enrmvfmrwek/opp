package com.opp.OrganizacijaFestivala.service.impl;

import com.opp.OrganizacijaFestivala.model.Event;
import com.opp.OrganizacijaFestivala.model.Festival;
import com.opp.OrganizacijaFestivala.model.OrganizationApplication;
import com.opp.OrganizacijaFestivala.model.RegisteredUser;
import com.opp.OrganizacijaFestivala.repository.EventRepository;
import com.opp.OrganizacijaFestivala.repository.FestivalRepository;
import com.opp.OrganizacijaFestivala.repository.OrganizationApplicationRepository;
import com.opp.OrganizacijaFestivala.repository.UserRepository;
import com.opp.OrganizacijaFestivala.rest.dto.FestivalDTO;
import com.opp.OrganizacijaFestivala.rest.dto.FestivalDTO2;
import com.opp.OrganizacijaFestivala.service.EventService;
import com.opp.OrganizacijaFestivala.service.FestivalService;
import com.opp.OrganizacijaFestivala.service.OrgAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class FestivalServiceJpa implements FestivalService {
    @Autowired
    FestivalRepository festivalRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    EventService eventService;
    @Autowired
    EventRepository eventRepository;
    @Autowired
    OrgAppService orgAppService;
    @Autowired
    OrganizationApplicationRepository organizationApplicationRepository;

    @Override
    public List<Festival> findAll() {
        return festivalRepository.findAll();
    }


    @Override
    public List<Festival> findAllByLeader(Long leaderId) {
        Optional<RegisteredUser> leader = userRepository.findById(leaderId);
        return festivalRepository.findAllByLeader(leader.get());
    }

    @Override
    public void delete(Long id) {
        List<Event> events = eventRepository.findAllByFestivalId(id);
        for (Event event : events) {
            eventService.delete(event.getId());
        }
        List<OrganizationApplication> organizationApplications = organizationApplicationRepository.findAllByFestivalId(id);
        for (OrganizationApplication organizationApplication : organizationApplications) {
            orgAppService.delete(organizationApplication.getId());
        }
        festivalRepository.deleteById(id);
    }

    @Override
    public boolean exists(Long id) {
        return festivalRepository.existsById(id);
    }

    @Override
    public Festival createFestival(FestivalDTO festivalDto) {
        Assert.isTrue(festivalDto.getLeader().length() > 0, "Festival se ne moze stvoriti bez voditelja");
        RegisteredUser leader = userRepository.findByUsername(festivalDto.getLeader());
        Assert.notNull(leader, "Ne postoji voditelj " + festivalDto.getLeader());
        Assert.isTrue(leader.getRole().equals("voditelj"), "Korisnik " + festivalDto.getLeader() +
                " nije voditelj nego " + leader.getRole());
        Assert.isTrue(festivalDto.getFestivalName().length() > 0, "Ime festivala mora biti zadano");
        Assert.notNull(festivalDto.getStartTime(), "Vrijeme početka mora biti zadano");
        Assert.notNull(festivalDto.getEndTime(), "Vrijeme završetka mora biti zadano");
        LocalDateTime dateTime = LocalDateTime.now().plusHours(1);
        Assert.isTrue(festivalDto.getStartTime().isAfter(dateTime), "Ne može se stvoriti festival koji je već počeo");
        Assert.isTrue(festivalDto.getStartTime().isBefore(festivalDto.getEndTime()),
                "Vrijeme završetka mora biti nakon vremena početka");
        Festival festival = new Festival(festivalDto, leader);
        return festivalRepository.save(festival);
    }

    @Override
    public Festival findById(Long id) {
        Optional<Festival> festival = festivalRepository.findById(id);
        Assert.isTrue(festival.isPresent(), "Ne postoji festival s tim id-jem");
        return festival.get();
    }

    @Override
    public FestivalDTO2 findById2(Long id) {
        Optional<Festival> festival = festivalRepository.findById(id);
        Assert.isTrue(festival.isPresent(), "Ne postoji festival s tim id-jem");
        return new FestivalDTO2(festival.get(), festival.get().getEndTime().isBefore(LocalDateTime.now().plusHours(1)));
    }

    @Override
    public List<Festival> findAllByActive(boolean isActive) {
        List<Festival> festivals = festivalRepository.findAll();
        List<Festival> activeFestivals = new ArrayList<>();
        LocalDateTime dateTime = LocalDateTime.now().plusHours(1);
        if (isActive) {
            //vraca festivale koji jos traju ili nisu poceli
            for (Festival festival : festivals) {
                if (festival.getEndTime().isAfter(dateTime)) {
                    activeFestivals.add(festival);
                }
            }
        } else {
            //vraca festivale koji su zavrsili
            for (Festival festival : festivals) {
                if (festival.getEndTime().isBefore(dateTime)) {
                    activeFestivals.add(festival);
                }
            }
        }
        return activeFestivals;
    }

    @Override
    public List<FestivalDTO2> findAllByLeader2(Long id) {
        Optional<RegisteredUser> leader = userRepository.findById(id);
        List<Festival> festivals = festivalRepository.findAllByLeader(leader.get());
        List<FestivalDTO2> festivalDTO2s = new ArrayList<>();
        for (Festival festival : festivals) {
            festivalDTO2s.add(new FestivalDTO2(festival, festival.getEndTime().isBefore(LocalDateTime.now().plusHours(1))));
        }
        return festivalDTO2s;
    }


}
