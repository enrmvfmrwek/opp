package com.opp.OrganizacijaFestivala.service;

import com.opp.OrganizacijaFestivala.model.OrganizationApplication;
import com.opp.OrganizacijaFestivala.model.RegisteredUser;
import com.opp.OrganizacijaFestivala.rest.dto.OrgAppDTO;
import com.opp.OrganizacijaFestivala.rest.dto.OrgAppDTO2;

import java.util.List;

public interface OrgAppService {
    List<OrganizationApplication> findAll();
    List<RegisteredUser> findConfirmed(Long id);
    List<OrgAppDTO2> findUnconfirmed(Long id);
    List<OrganizationApplication> findUserOrgApp(Long userId);
    void delete(Long id);
    boolean exists(Long id);
    OrganizationApplication createOrgApp(OrgAppDTO dto);
    OrganizationApplication confirm(Long id);
    List<OrgAppDTO2> organizersUnconfirmedApps(Long organizerId);
}
