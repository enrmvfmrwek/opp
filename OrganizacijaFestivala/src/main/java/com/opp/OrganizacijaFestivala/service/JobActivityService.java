package com.opp.OrganizacijaFestivala.service;

import com.opp.OrganizacijaFestivala.model.Activity;
import com.opp.OrganizacijaFestivala.model.Job_Activity;
import com.opp.OrganizacijaFestivala.rest.dto.JobActivityDTO;

import java.util.List;

public interface JobActivityService {
    List<Activity> findAllByJobId(Long id);
    boolean delete(Long jId, Long aId);
    Job_Activity create(Long jId, Long aId);
    boolean delete(Job_Activity job_activity);
}
