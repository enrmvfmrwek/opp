package com.opp.OrganizacijaFestivala.service.impl;

import com.opp.OrganizacijaFestivala.model.*;
import com.opp.OrganizacijaFestivala.repository.*;
import com.opp.OrganizacijaFestivala.rest.dto.JobActivityDTO;
import com.opp.OrganizacijaFestivala.rest.dto.JobDTO;
import com.opp.OrganizacijaFestivala.rest.dto.JobDTO2;
import com.opp.OrganizacijaFestivala.service.JobActivityService;
import com.opp.OrganizacijaFestivala.service.JobAppService;
import com.opp.OrganizacijaFestivala.service.JobService;
import io.jsonwebtoken.lang.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class JobServiceJpa implements JobService {
    @Autowired
    JobRepository jobRepository;

    @Autowired
    ActivityRepository activityRepository;

    @Autowired
    EventRepository eventRepository;

    @Autowired
    JobActivityRepo jobActivityRepo;

    @Autowired
    JobActivityService jobActivityService;
    @Autowired
    JobApplicationRepository jobApplicationRepository;
    @Autowired
    JobAppService jobAppService;
    @Autowired
    FestivalRepository festivalRepository;

    @Override
    public Job createJob(JobDTO jobDTO) {
        Optional<Event> event = eventRepository.findById(jobDTO.getEventId());
        Assert.isTrue(event.isPresent(), "Događanje mora postojati");
        int ordNum = jobDTO.getOrdinalNumber();
        Assert.isTrue(ordNum>0, "Redni broj posla mora biti prirodan broj, a ne "
                + ordNum);
        if(ordNum>1){
            List<Job> previousJobs = jobRepository.findAllByEventIdAndOrdinalNumber(jobDTO.getEventId(), ordNum-1);
            Assert.isTrue(!previousJobs.isEmpty(), "Ne može se stvoriti posao s rednim brojem "+ordNum
            +" prije posla s rednim brojem "+(ordNum-1));
        }
        String[] activityNames = jobDTO.getActivities();
        Assert.isTrue((activityNames.length>0), "Posao se sastoji od barem jedne aktivnosti");
        Job job = new Job(jobDTO);
        job = jobRepository.save(job);
        Activity activity;
        for(int i=0; i<activityNames.length; i++){
            activity = activityRepository.findByActivityName(activityNames[i]);
            jobActivityRepo.save(new Job_Activity(job.getJobId(), activity.getActivityId()));
        }
        return job;

    }

    @Override
    public Job getJobById (Long id) {return checkAuctionOver(jobRepository.findById(id).get());}

    @Override
    public List<JobDTO2> getOpenAuctions2() {
        checkAuctionOver(jobRepository.findAllByAuctionOpen(true));
        List<Job> jobs = jobRepository.findAllByAuctionOpen(true);
        List<JobDTO2> jobActivities = new ArrayList<>();
        for(Job job: jobs) {
            Optional<Event> event = eventRepository.findById(job.getEventId());
            Assert.isTrue(event.isPresent(), "Ne postoji događanje kojem pripada taj posao");
            Optional<Festival> festival = festivalRepository.findById(event.get().getFestivalId());
            Assert.isTrue(festival.isPresent(), "Ne postoji festival kojem pripada taj posao");
            List<Job_Activity> job_activities = jobActivityRepo.findAllByJobId(job.getJobId());
            Activity activity;
            List<Activity> activityList = new ArrayList<>();
            for (Job_Activity j : job_activities) {
                activity = activityRepository.findByActivityId(j.getActivityId());
                Assert.notNull(activity, "Posao ima aktivnost koja ne postoji u bazi");
                activityList.add(activity);
            }
            jobActivities.add(new JobDTO2(job, event.get().getEventName(),
                    festival.get().getFestivalName(), activityList));
        }
        return jobActivities;
    }

    @Override
    public Job openAuction(Long id) {
        Optional<Job> job = jobRepository.findById(id);
        Assert.isTrue(job.isPresent(), "Posao za koji se pokušava otvoriti licitacija ne postoji.");
        job.get().setAuctionOpen(true);
        job.get().setAuctionEndTime(LocalDateTime.now().plusHours(1).plusDays(1));
        //kada se updatea nesto potrebno je opet isto to spremiti u bazu
        return jobRepository.save(job.get());
    }

    @Override
    public Job closeAuction(Long id) {
        Optional<Job> job = jobRepository.findById(id);
        Assert.isTrue(job.isPresent(), "Posao za koji se pokušava zatvoriti licitacija ne postoji.");
        job.get().setAuctionOpen(false);
        List<JobApplication> applications = jobAppService.findConfirmed(id);
        JobApplication application = minPrice(applications);
        if(application!=null){
            job.get().setWinAppId(application.getId());
        }
        return jobRepository.save(job.get());
    }

    @Override
    public Job closeAuction(Job job) {
        job.setAuctionOpen(false);
        List<JobApplication> applications = jobAppService.findConfirmed(job.getJobId());
        JobApplication application = minPrice(applications);
        if(application!=null){
            job.setWinAppId(application.getId());
        }
        return jobRepository.save(job);
    }

    public static JobApplication minPrice(List<JobApplication> applications){
        JobApplication application = null;
        for(JobApplication app: applications){
            if(application==null || application.getPrice()>app.getPrice()) {
                application = app;
            }
        }
        return application;
    }

    @Override
    public List<Job> getOpenAuctions() {
        checkAuctionOver(jobRepository.findAllByAuctionOpen(true));
        return jobRepository.findAllByAuctionOpen(true);
    }

    @Override
    public List<Job> eventJobs(Long id) {
        return checkAuctionOver(jobRepository.findAllByEventId(id));
    }

    @Override
    public List<JobDTO2> eventJobs2(Long id) {
        List<Job> jobs = checkAuctionOver(jobRepository.findAllByEventId(id));
        List<JobDTO2> jobActivities = new ArrayList<>();
        for(Job job: jobs) {
            Optional<Event> event = eventRepository.findById(job.getEventId());
            Assert.isTrue(event.isPresent(), "Ne postoji događanje kojem pripada taj posao");
            Optional<Festival> festival = festivalRepository.findById(event.get().getFestivalId());
            Assert.isTrue(festival.isPresent(), "Ne postoji festival kojem pripada taj posao");
            List<Job_Activity> job_activities = jobActivityRepo.findAllByJobId(job.getJobId());
            Activity activity;
            List<Activity> activityList = new ArrayList<>();
            for (Job_Activity j : job_activities) {
                activity = activityRepository.findByActivityId(j.getActivityId());
                Assert.notNull(activity, "Posao ima aktivnost koja ne postoji u bazi");
                activityList.add(activity);
            }
            jobActivities.add(new JobDTO2(job, event.get().getEventName(),
                    festival.get().getFestivalName(), activityList));
        }
        return jobActivities;
    }


    @Override
    public List<JobActivityDTO> getJobsAndActivities(Long eventId) {
        List<Job> jobs = jobRepository.findAllByEventId(eventId);
        List<JobActivityDTO> jobActivities = new ArrayList<>();
        for(Job job: jobs){
            List<Job_Activity> job_activities = jobActivityRepo.findAllByJobId(job.getJobId());
            Activity activity;
            List<Activity> activityList = new ArrayList<>();
            for(Job_Activity j: job_activities){
                activity = activityRepository.findByActivityId(j.getActivityId());
                Assert.notNull(activity, "Posao ima aktivnost koja ne postoji u bazi");
                activityList.add(activity);
            }
            jobActivities.add(new JobActivityDTO(job, activityList));
        }
        return jobActivities;
    }

    //poziva se prije svakog get requesta da se provjeri je li licitacija istekla te da se zatvori
    public List<Job> checkAuctionOver(List<Job> jobs){
        List<Job> jobs2 = new ArrayList<>();
        for(Job job: jobs){
            if(job.isAuctionOpen()==true && job.getAuctionEndTime().isBefore(LocalDateTime.now().plusHours(1))){
                jobs2.add(closeAuction(job));
            } else{
                jobs2.add(job);
            }
        }
        return jobs2;
    }

    public Job checkAuctionOver(Job job){
        if(job.isAuctionOpen()==true && job.getAuctionEndTime().isBefore(LocalDateTime.now().plusHours(1))) {
            return closeAuction(job);
        }
        else{
            return job;
        }
    }

    @Override
    public void delete(Long id) {
        List<Job_Activity> job_activities = jobActivityRepo.findAllByJobId(id);
        for(Job_Activity job_activity: job_activities){
            jobActivityService.delete(job_activity);
        }
        List<JobApplication> jobApplications = jobApplicationRepository.findAllByJobId(id);
        for(JobApplication jobApplication: jobApplications){
            jobAppService.delete(jobApplication);
        }
        jobRepository.deleteById(id);
    }
}
