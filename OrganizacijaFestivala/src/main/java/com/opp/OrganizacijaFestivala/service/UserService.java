package com.opp.OrganizacijaFestivala.service;

import com.opp.OrganizacijaFestivala.model.Performer_Activity;
import com.opp.OrganizacijaFestivala.model.RegisteredUser;
import com.opp.OrganizacijaFestivala.rest.dto.UserDTO;
import com.opp.OrganizacijaFestivala.exception.EmailExistsException;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface UserService {
    List<RegisteredUser> findAllByRole(String role);
    RegisteredUser findByUsername(String username);
    String registerNewUserAccount(UserDTO accountDto) throws EmailExistsException;
    public String login(String username, String password);
    RegisteredUser updateUser(Long id);
    public void delete(Long id);
    public RegisteredUser whoami(HttpServletRequest req);
    public String refresh(String username);

    List<RegisteredUser> findAll();

    RegisteredUser currentUser(String token);

    RegisteredUser findById(Long id);

}