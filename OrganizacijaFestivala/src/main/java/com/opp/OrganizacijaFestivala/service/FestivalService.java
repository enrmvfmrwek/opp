package com.opp.OrganizacijaFestivala.service;

import com.opp.OrganizacijaFestivala.model.Festival;
import com.opp.OrganizacijaFestivala.rest.dto.FestivalDTO;
import com.opp.OrganizacijaFestivala.rest.dto.FestivalDTO2;

import java.util.List;

public interface FestivalService {

    List<Festival> findAll();

    List<Festival> findAllByLeader(Long leader);
    void delete(Long id);
    boolean exists(Long id);
    Festival createFestival(FestivalDTO festivalDTO);
    Festival findById(Long id);
    public FestivalDTO2 findById2(Long id);

    List<Festival> findAllByActive(boolean isActive);

    List<FestivalDTO2> findAllByLeader2(Long id);
}
