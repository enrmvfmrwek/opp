package com.opp.OrganizacijaFestivala.exception;

public class InvalidRoleException extends Throwable{

    public InvalidRoleException(String message) {
        super(message);
    }
}
