package com.opp.OrganizacijaFestivala.security;

import com.opp.OrganizacijaFestivala.repository.UserRepository;
import com.opp.OrganizacijaFestivala.model.RegisteredUser;
import com.opp.OrganizacijaFestivala.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.security.core.userdetails.User;

import java.util.List;

import static org.springframework.security.core.authority.AuthorityUtils.commaSeparatedStringToAuthorityList;

@Service
@Transactional
public class MyUserDetailsService implements UserDetailsService {
    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;
    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
        RegisteredUser user = userRepository.findByUsername(username);
        if (user == null) {
            System.out.println("No user '" + username + "'");
            throw new UsernameNotFoundException(
                    "No user found with username: "+ username);
        }

        boolean enabled = true;
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;
        User korisnik = null;

        List<GrantedAuthority> authorities = getAuthorities(username, user.getRole());
        if(authorities == null){
            return null;
        }
        //System.out.println("Uspjesno potvrden: "+user.getUsername()+" "+authorities);
        korisnik =  new User
                (user.getUsername(),
                        user.getPassword(), enabled, accountNonExpired,
                        credentialsNonExpired, accountNonLocked,
                        authorities);
        return korisnik;

    }

    public static List<GrantedAuthority> getAuthorities(String username, String role)  {
        List<GrantedAuthority> authorities = null;
        if ("admin".equals(username) || "admin".equals(role)){
            authorities = commaSeparatedStringToAuthorityList("ROLE_ADMIN");
        }
        else if (role.equals("voditelj")) {
            authorities = commaSeparatedStringToAuthorityList("ROLE_LEADER");
        }
        else if (role.equals("organizator"))
            authorities = commaSeparatedStringToAuthorityList("ROLE_ORGANIZER");
        else if (role.equals("izvodjac"))
            authorities = commaSeparatedStringToAuthorityList("ROLE_PERFORMER");
        else if (role.equals("nepotvrdjeni_voditelj"))
            authorities = commaSeparatedStringToAuthorityList("ROLE_UNCONFIRMED_LEADER");
        else {
            System.err.println(role + " nije dozvoljena uloga!");
        }
        return authorities;
    }


}

