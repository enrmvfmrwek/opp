package com.opp.OrganizacijaFestivala.service;

import com.opp.OrganizacijaFestivala.model.Festival;
import com.opp.OrganizacijaFestivala.model.RegisteredUser;
import com.opp.OrganizacijaFestivala.repository.FestivalRepository;
import com.opp.OrganizacijaFestivala.repository.UserRepository;
import com.opp.OrganizacijaFestivala.rest.dto.FestivalDTO;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest
class FestivalServiceTest {

    @Autowired
    private FestivalService festivalService;

    @MockBean
    private FestivalRepository festivalRepository;

    @MockBean
    private UserRepository userRepository;


    @Nested
    class CreateFestivalTest {

        @Test
        void emptyFormsTest() {
            Mockito.when(userRepository.findByUsername("leaderName")).thenReturn(new RegisteredUser("leaderName", "voditelj"));

            FestivalDTO f1 = new FestivalDTO("", "festName", "", LocalDateTime.now().plusHours(1), LocalDateTime.now().plusHours(2));
            FestivalDTO f2 = new FestivalDTO("leaderName", "", "", LocalDateTime.now().plusHours(1), LocalDateTime.now().plusHours(2));
            FestivalDTO f3 = new FestivalDTO("leaderName", "festName", "", null, LocalDateTime.now().plusHours(2));
            FestivalDTO f4 = new FestivalDTO("leaderName", "festName", "", LocalDateTime.now().plusHours(1), null);
            FestivalDTO f5 = new FestivalDTO("leaderName", "festName", "desc", LocalDateTime.now().plusHours(1), LocalDateTime.now().plusHours(2));

            assertAll(
                    () -> assertThrows(IllegalArgumentException.class, () -> festivalService.createFestival(f1)),
                    () -> assertThrows(IllegalArgumentException.class, () -> festivalService.createFestival(f2)),
                    () -> assertThrows(IllegalArgumentException.class, () -> festivalService.createFestival(f3)),
                    () -> assertThrows(IllegalArgumentException.class, () -> festivalService.createFestival(f4)),
                    () -> assertDoesNotThrow(() -> festivalService.createFestival(f5))
            );
        }

        @Test
        void dateTimeTest() {
            Mockito.when(userRepository.findByUsername("l")).thenReturn(new RegisteredUser("l", "voditelj"));

            FestivalDTO f1 = new FestivalDTO("l", "n", "", LocalDateTime.now().minusMinutes(5), LocalDateTime.now().plusHours(2));
            FestivalDTO f2 = new FestivalDTO("l", "n", "", LocalDateTime.now().plusMinutes(5), LocalDateTime.now().minusMinutes(2));
            FestivalDTO f3 = new FestivalDTO("l", "n", "", LocalDateTime.now().plusMinutes(5), LocalDateTime.now().plusMinutes(2));
            FestivalDTO f4 = new FestivalDTO("l", "n", "", LocalDateTime.now().plusMinutes(5), LocalDateTime.now().plusMinutes(7));

            assertAll(
                    () -> assertThrows(IllegalArgumentException.class, () -> festivalService.createFestival(f1)),
                    () -> assertThrows(IllegalArgumentException.class, () -> festivalService.createFestival(f2)),
                    () -> assertThrows(IllegalArgumentException.class, () -> festivalService.createFestival(f3)),
                    () -> assertDoesNotThrow(() -> festivalService.createFestival(f4))
            );
        }

        @Test
        void leaderTest() {
            Mockito.when(userRepository.findByUsername("lead2")).thenReturn(new RegisteredUser("l2", "ne_voditelj"));
            Mockito.when(userRepository.findByUsername("lead3")).thenReturn(new RegisteredUser("l3", "voditelj"));

            FestivalDTO f1 = new FestivalDTO("lead1", "n", "", LocalDateTime.now().plusHours(5), LocalDateTime.now().plusHours(7));
            FestivalDTO f2 = new FestivalDTO("lead2", "n", "", LocalDateTime.now().plusHours(5), LocalDateTime.now().plusHours(7));
            FestivalDTO f3 = new FestivalDTO("lead3", "n", "", LocalDateTime.now().plusHours(5), LocalDateTime.now().plusHours(7));

            assertAll(
                    () -> assertThrows(IllegalArgumentException.class, () -> festivalService.createFestival(f1)), // voditelj ne postoji u bazi
                    () -> assertThrows(IllegalArgumentException.class, () -> festivalService.createFestival(f2)), // korisnik nije voditelj
                    () -> assertDoesNotThrow(() -> festivalService.createFestival(f3))
            );
        }

        @Test
        void addingFestivalToRepoTest() {
            String lName = "l1";
            String fName = "n";
            String desc = "d";
            LocalDateTime start = LocalDateTime.now().plusHours(5);
            LocalDateTime end = LocalDateTime.now().plusHours(7);
            Festival f1 = new Festival(
                    new FestivalDTO(lName, fName, desc, start, end),
                    new RegisteredUser(lName, "voditelj")
            );

            Mockito.when(festivalRepository.save(any(Festival.class))).thenReturn(f1);
            Mockito.when(userRepository.findByUsername(lName)).thenReturn(new RegisteredUser(lName, "voditelj"));

            assertDoesNotThrow(() -> festivalService.createFestival(new FestivalDTO(f1)));

            Festival fReturn = festivalService.createFestival(new FestivalDTO(f1));
            assertAll(
                    () -> assertEquals(lName, fReturn.getLeader().getUsername()),
                    () -> assertEquals(fName, fReturn.getFestivalName()),
                    () -> assertEquals(desc, fReturn.getDescription()),
                    () -> assertEquals(start, fReturn.getStartTime()),
                    () -> assertEquals(end, fReturn.getEndTime())
            );
        }
    }


    @Nested
    class FindByIdTest {

        @Test
        void idNullTest() {
            assertThrows(IllegalArgumentException.class, () -> festivalService.findById(null));
        }

        @Test
        void idExistsInRepo() {
            Mockito.when(festivalRepository.findById((long) 12)).thenReturn(Optional.of(new Festival()));

            assertThrows(IllegalArgumentException.class, () -> festivalService.findById((long) 41));
            assertDoesNotThrow(() -> festivalService.findById((long) 12));
        }
    }
}